'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('exercise_in_program', {
            id: {
                type: Sequelize.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },

            exercise_id: {
                type: Sequelize.INTEGER.UNSIGNED,
                allowNull: false,
                references: {
                    model: 'exercises',
                    key: 'id'
                },
                onDelete: 'cascade'
            },

            program_id: {
                type: Sequelize.INTEGER.UNSIGNED,
                allowNull: false,
                references: {
                    model: 'programs',
                    key: 'id'
                },
                onDelete: 'cascade'
            },
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('exercise_in_program');
    }
};
