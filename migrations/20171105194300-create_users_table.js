'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('users', {
            id: {
                type: Sequelize.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },

            email: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            first_name: {
                type: Sequelize.STRING,
                allowNull: true
            },
            last_name: {
                type: Sequelize.STRING,
                allowNull: true
            },

            role: {
                type: Sequelize.ENUM('trainer', 'trainee'),
                allowNull: false,
                defaultValue: 'trainee'
            },

            password: {
                type: Sequelize.STRING,
                allowNull: true,
                defaultValue: null
            },

            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: false
            },
            deleted_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: null
            },
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('users');

    }
};
