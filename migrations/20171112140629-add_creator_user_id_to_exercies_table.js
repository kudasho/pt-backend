'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.addColumn(
            'exercises',
            'creator_user_id',
            {
                type: Sequelize.INTEGER.UNSIGNED,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                },
                onDelete: 'cascade'
            }
        )
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.removeColumn('exercises', 'creator_user_id');
    }
};
