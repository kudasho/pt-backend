const gulp = require('gulp');
const ts = require('gulp-typescript');
const nodemon = require('gulp-nodemon');
const del = require('del');

const tsProject = ts.createProject('tsconfig.json');

gulp.task('compile', ['clean'], () => {
    const tsResult = tsProject.src().pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('clean', function () {
    return del([
        'dist/'
    ]);
});

gulp.task('watch', () => {
    gulp.watch('src/**/*.ts', ['compile']);
});

gulp.task('serve', ['compile', 'watch'], () => {
    nodemon({
        script: 'dist/index.js',
        env: {
            NODE_ENV: 'development',
            DEBUG: 'pt-backend:*'
        },
        delay: 500
    });
});

gulp.task('default', ['serve']);