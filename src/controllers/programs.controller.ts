import { BaseController } from './base.controller';
import { default as TYPES, IProgramsService, ITrainingService } from '../services/types';
import container from '../services/container';

export class ProgramsController extends BaseController {
    protected programsService = container.get<IProgramsService>(TYPES.ProgramsService);
    protected trainingService = container.get<ITrainingService>(TYPES.TrainingService);

    async getProgramsListAction() {
        const user = this.request.auth_user;

        const programsList = await this.programsService.getProgramsList(user);

        return this.makeResponse(programsList);
    }

    async createProgramAction() {
        const data = this.request.body;

        const program = await this.trainingService.startNewProgram({
                title: data.title,
                description: data.description
            },
            this.request.auth_user
        );

        return this.makeResponse(program);
    }

    async showProgramAction() {
        const programId = this.request.params.program_id;

        const program = await this.programsService.getProgramById(Number(programId));

        return this.makeResponse(program);
    }

    async updateProgramAction() {
        const programId = this.request.params.program_id;
        const data = this.request.body;

        const program = await this.programsService.updateProgramById(Number(programId), data);

        return this.makeResponse(program);
    }

    async deleteProgramAction() {
        const programId = this.request.params.program_id;

        const result = await this.programsService.deleteProgramById(Number(programId));

        return this.makeResponse(result);
    }
}