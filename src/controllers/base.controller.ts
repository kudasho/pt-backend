import {RoutedController} from 'express-routed-controllers';

import * as express from 'express';
import {IUser} from "../typings";

interface IRequest extends express.Request {
    auth_user: IUser;
}

export class BaseController extends RoutedController {
    public request: IRequest;
}