import container from '../services/container';
import {IAuthService, default as TYPES, IUsersService} from "../services/types";
import {BaseController} from "./base.controller";
import {IUser} from "../typings";

export class AuthController extends BaseController {
    protected authService = container.get<IAuthService>(TYPES.AuthService);
    protected usersService = container.get<IUsersService>(TYPES.UsersService);

    me() {
        let token = this.request.cookies['auth'];

        if (!token) {
            return this.response.json({
                first_name: 'Guest',
            });
        }

        return this.authService.getUserByToken(token)
            .then(user => {
                this.makeResponse(user);
            })
    }

    register() {
        let data: IUser = {...this.request.body};

        return this.authService.register(data)
            .then(user => {
                // save auth token cookie
                let token = this.authService.generateAuthToken(user);
                this.response.cookie('auth', token);

                return this.makeResponse(user);
            });
    }

    login() {
        let data = this.request.body;

        let email = data.email;
        let password = data.password;

        if (!email) {
            throw new Error('no email provided');
        }

        if (!password) {
            throw new Error('no password provided');
        }

        return this.authService.login(email, password)
            .then(user => {
                // save auth token cookie
                let token = this.authService.generateAuthToken(user);
                this.response.cookie('auth', token);

                return this.makeResponse(user);
            })
    }

    logout() {
        this.response.clearCookie('auth');

        return this.makeResponse(true);
    }
}