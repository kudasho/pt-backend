import {BaseController} from "./base.controller";
import {default as TYPES, ITrainingService} from "../services/types";
import container from "../services/container";

export class TrainingController extends BaseController {
    protected trainingService = container.get<ITrainingService>(TYPES.TrainingService);

    addExerciseToProgram() {
        const programId = this.request.body.program_id;
        const exerciseId = this.request.body.exercise_id;

        return this.trainingService.addExerciseToProgram(
            Number(programId),
            Number(exerciseId)
        )
            .then(result => {
                return this.makeResponse(result);
            });
    }
}