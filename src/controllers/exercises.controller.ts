import {BaseController} from "./base.controller";
import {default as TYPES, IExercisesService, ITrainingService} from "../services/types";
import container from "../services/container";

export class ExercisesController extends BaseController {
    protected trainingService = container.get<ITrainingService>(TYPES.TrainingService);
    protected exercisesService = container.get<IExercisesService>(TYPES.ExercisesService);

    create() {
        const data = this.request.body;

        return this.trainingService.createNewExercise(
            {
                title: data.title,
                description: data.description,
                links: data.links
            },
            this.request.auth_user.id
        )
            .then(exercise => {
                return this.makeResponse(exercise);
            })
    }

    show() {
        const exerciseId = this.request.params.exercise_id;

        return this.exercisesService.getExerciseById(Number(exerciseId))
            .then(exercise => {
                return this.makeResponse(exercise);
            })
    }

    update() {
        const exerciseId = this.request.params.exercise_id;
        const data = this.request.body;

        return this.exercisesService.updateExerciseById(
            Number(exerciseId),
            data
        )
            .then(exercise => {
                return this.makeResponse(exercise);
            });
    }

    delete() {
        const exerciseId = this.request.params.exercise_id;

        return this.exercisesService.deleteExerciseById(Number(exerciseId))
            .then(result => {
                return this.makeResponse(result);
            })
    }
}