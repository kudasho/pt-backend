export interface IUser {
    id?: number;
    email?: string;
    first_name?: string;
    last_name?: string;
    password?: string;
    role?: 'trainer' | 'trainee';
}

export interface IExercise {
    id?: number;
    title?: string;
    description?: string;
    links?: string[];
    creator_user_id?: number;
    creator_user?: IUser;
    programs?: IProgram[];
}

export interface IExerciseInProgram {
    program_id?: number;
    exercise_id?: number;
}

export interface IProgram {
    id?: number;
    title?: string;
    description?: string;
    creator_user_id?: number;
    creator_user?: IUser;
    exercises?: IExercise[];
}