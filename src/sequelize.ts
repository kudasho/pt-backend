import * as config from "config";
import {Sequelize} from 'sequelize-typescript';

export const sequelize = new Sequelize({
    name: config.get('DB.database'),
    dialect: config.get('DB.dialect'),
    username: config.get('DB.username'),
    password: config.get('DB.password'),
    modelPaths: [__dirname + '/models']
});