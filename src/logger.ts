import * as debug from 'debug';

export const debugLogger = {
    server: debug('pt-backend:server')
};