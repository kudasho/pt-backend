import { DynamicRouter } from 'express-routed-controllers';
import { AuthController } from './controllers/auth.controller';

import { authMiddleware } from './middlewares/auth.middleware';
import { guestMiddleware } from './middlewares/guest.middleware';
import { ProgramsController } from './controllers/programs.controller';
import { ExercisesController } from './controllers/exercises.controller';
import { TrainingController } from './controllers/training.controller';

export const dRouter = new DynamicRouter();

dRouter.group({prefix: '/api'}, (api) => {
    api.group({
            middlewares: [guestMiddleware]
        },
        guest => {
            guest.post('/auth/register', AuthController, 'register');
            guest.post('/auth/login', AuthController, 'login');
            guest.post('/auth/logout', AuthController, 'logout');
        }
    );

    api.group({
            middlewares: [authMiddleware]
        },
        auth => {
            auth.get('/auth/me', AuthController, 'me');

            auth.group({prefix: '/programs'}, programs => {
                programs.get('/', ProgramsController, 'getProgramsListAction');
                programs.post('/', ProgramsController, 'createProgramAction');
                programs.get('/:program_id(\\d+)', ProgramsController, 'showProgramAction');
                programs.put('/:program_id(\\d+)', ProgramsController, 'updateProgramAction');
                programs.delete('/:program_id(\\d+)', ProgramsController, 'deleteProgramAction');
            });

            auth.group({prefix: '/exercises'}, exercises => {
                exercises.post('/', ExercisesController, 'create');
                exercises.get('/:exercise_id(\\d+)', ExercisesController, 'show');
                exercises.put('/:exercise_id(\\d+)', ExercisesController, 'update');
                exercises.delete('/:exercise_id(\\d+)', ExercisesController, 'delete');
            });

            auth.group({prefix: '/training'}, training => {
                training.post('/add-exercise-to-program', TrainingController, 'addExerciseToProgram');
            })
        }
    );

});

