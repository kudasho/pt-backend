import container from '../services/container';
import { IAuthService, default as TYPES } from '../services/types';

export function authMiddleware(req, res, next) {
    const authService = container.get<IAuthService>(TYPES.AuthService);

    const token = req.cookies['auth'];

    if (!token) {
        return res.status(401).json('Endpoint is not allowed for guest');
    }

    return authService.getUserByToken(token)
        .then(user => {
            if (!user) {
                return res.status(401).json('Guests are not allowed');
            }

            req.auth_user = user;

            return next();
        })
        .catch(next);
}