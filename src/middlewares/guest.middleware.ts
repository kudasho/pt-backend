import container from "../services/container";
import {IAuthService, default as TYPES} from "../services/types";

export function guestMiddleware(req, res, next) {
    const authService = container.get<IAuthService>(TYPES.AuthService);
    const token = req.cookies['auth'];

    if (!token) {
        return next();
    }

    return authService.getUserByToken(token)
        .then(user => {
            if (user) {
                throw new Error('Endpoint is not allowed for authenticated user');
            }

            return next();
        })
        .catch(err => {
            console.error(err);

            // allow proceed if jwt expired
            return next();
        });
}