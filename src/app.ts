import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';


import {sequelize} from "./sequelize";
import {dRouter} from "./router";
import {debugLogger} from "./logger";
import { ValidationError } from 'sequelize';

class App {
    public express: express.Application = express();

    constructor() {
        this.initMiddlewares();
        this.initSequelize();
        this.initRoutes();

        this.express.use((err, req, res, next) => {
            return this.handleError(err, req, res);
        });
        this.express.use(this.notFoundHandler);
    }

    private initMiddlewares(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({extended: false}));
        this.express.use(cookieParser());


        debugLogger.server('Middlewares were initiated.');
    }

    private async initSequelize() {
        await sequelize.authenticate();

        debugLogger.server('Connection to DB has been established successfully.');
    }

    private initRoutes() {
        this.express.use(dRouter.router);

        debugLogger.server('Routes were initiated.');
    }

    /**
     * @todo incomingError can be a simple Error object. find way to catch required error class
     */
    protected handleError(incomingError: ValidationError, req, res) {
        console.error(incomingError);

        let message = incomingError.message;

        // sequelize validation errors
        if (incomingError.errors) {
            incomingError.errors.forEach(err => message = err.message + ';');
        }

        return res.status(500).json(message);
    }

    protected notFoundHandler(req, res, next) {
        return res.status(404).json('Not found')
    }
}

export default new App().express;