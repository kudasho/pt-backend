import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { DataType } from 'sequelize-typescript';

import * as passwordHash from 'password-hash';
import { IUser } from '../typings';

export const ROLE_TRAINER = 'trainer';
export const ROLE_TRAINEE = 'trainee';

@Table({
    tableName: 'users'
})
export default class UserModel extends Model<UserModel> implements IUser {
    @Column({
        type: DataType.TEXT,
        allowNull: false
    })
    email;

    @Column({
        type: DataType.TEXT,
        allowNull: true,
        defaultValue: null
    })
    first_name;

    @Column({
        type: DataType.TEXT,
        allowNull: true,
        defaultValue: null
    })
    last_name;

    @Column({
        set(val) {
            if (!val) {
                throw new Error('Empty password not allowed');
            }

            this.setDataValue('password', passwordHash.generate(val));
        },
        type: DataType.TEXT,
        allowNull: false
    })
    password;

    @Column({
        type: DataType.ENUM(ROLE_TRAINER, ROLE_TRAINEE),
        allowNull: false,
        defaultValue: ROLE_TRAINEE
    })
    role;
}