import {Table, Column, Model, HasMany, ForeignKey, BelongsTo, BelongsToMany} from 'sequelize-typescript';
import {DataType} from 'sequelize-typescript';
import {default as UserModel} from "./user.model";
import ExerciseInProgramModel from "./exercise-in-program.model";
import ExerciseModel from "./exercise.model";
import {IProgram} from "../typings";

@Table({
    tableName: 'programs'
})
export default class ProgramModel extends Model<ProgramModel> implements IProgram {
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    title;

    @Column({
        type: DataType.TEXT,
        allowNull: false
    })
    description;

    @ForeignKey(() => UserModel)
    creator_user_id;

    @BelongsTo(() => UserModel)
    creator_user: UserModel;

    @BelongsToMany(() => ExerciseModel, () => ExerciseInProgramModel)
    exercises: ExerciseModel[];
}