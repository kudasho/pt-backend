import {Table, Column, Model, HasMany, ForeignKey, BelongsTo} from 'sequelize-typescript';
import ProgramModel from "./program.model";
import ExerciseModel from "./exercise.model";
import {IExerciseInProgram} from "../typings";


@Table({
    tableName: 'exercise_in_program'
})
export default class ExerciseInProgramModel extends Model<ExerciseInProgramModel> implements IExerciseInProgram {
    @ForeignKey(() => ProgramModel)
    program_id;

    @ForeignKey(() => ExerciseModel)
    exercise_id;
}