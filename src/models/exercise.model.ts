import {Table, Column, Model, HasMany, ForeignKey, BelongsTo, BelongsToMany} from 'sequelize-typescript';
import {DataType} from 'sequelize-typescript';
import {default as UserModel} from "./user.model";
import ExerciseInProgramModel from "./exercise-in-program.model";
import ProgramModel from "./program.model";
import {IExercise} from "../typings";

@Table({
    tableName: 'exercises'
})
export default class ExerciseModel extends Model<ExerciseModel> implements IExercise {
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    title;

    @Column({
        type: DataType.TEXT,
        allowNull: false
    })
    description;

    @Column({
        type: DataType.ARRAY(DataType.STRING),
        allowNull: true,
        get: function () {
            const val = this.getDataValue('links');

            return val ? val.split(';') : [];
        },
        set: function (val) {
            if (!val) {
                val = null;
            } else {
                val = val.join(';');
            }

            this.setDataValue('links', val);
        }
    })
    links;

    @ForeignKey(() => UserModel)
    creator_user_id;

    @BelongsTo(() => UserModel)
    creator_user: UserModel;

    @BelongsToMany(() => ProgramModel, () => ExerciseInProgramModel)
    programs: ProgramModel[];
}