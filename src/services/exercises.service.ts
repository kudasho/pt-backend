import {injectable} from "inversify";
import {IExercisesService} from "./types";

import ExerciseModel from "../models/exercise.model";
import {IExercise} from "../typings";

@injectable()
export class ExerciseService implements IExercisesService {
    async createExercise(data: IExercise) {
        const exerciseInstance = await ExerciseModel.create(data);

        return exerciseInstance.get();
    }

    async updateExerciseById(id: number, data: IExercise) {
        const exerciseInstance = await ExerciseModel.findById(id);

        if (!exerciseInstance) {
            throw new Error(`Exercise #${id} was not found`);
        }

        return exerciseInstance.update(data);
    }

    async deleteExerciseById(id: number) {
        await ExerciseModel.destroy({
            where: {
                id: id
            }
        });

        return true;
    }

    async getExerciseById(id: number) {
        const exerciseInstance = await ExerciseModel.findById(id);

        if (!exerciseInstance) {
            throw new Error(`Exercise #${id} was not found`);
        }

        return exerciseInstance.get();
    }

    // getExercisesList(programId: number) {
    //     // todo get by program_id
    // }
}