import {Container} from "inversify";
import TYPES, {IAuthService, IExercisesService, IProgramsService, ITrainingService, IUsersService} from "./types";

import {AuthService} from "./auth.service";
import {UsersService} from "./users.service";
import {ProgramsService} from "./programs.service";
import {ExerciseService} from "./exercises.service";
import {TrainingService} from "./training.service";

const container = new Container();

container.bind<IAuthService>(TYPES.AuthService).to(AuthService);
container.bind<IUsersService>(TYPES.UsersService).to(UsersService);
container.bind<IProgramsService>(TYPES.ProgramsService).to(ProgramsService);
container.bind<IExercisesService>(TYPES.ExercisesService).to(ExerciseService);
container.bind<ITrainingService>(TYPES.TrainingService).to(TrainingService);

export default container;