import {inject, injectable} from "inversify";
import {default as TYPES, IExercisesService, IProgramsService, ITrainingService} from "./types";
import ExerciseInProgramModel from "../models/exercise-in-program.model";
import { IExercise, IProgram, IUser } from '../typings';

@injectable()
export class TrainingService implements ITrainingService {
    @inject(TYPES.ProgramsService) programsService: IProgramsService;
    @inject(TYPES.ExercisesService) exercisesService: IExercisesService;

    async startNewProgram(data: IProgram, creator: IUser) {
        data.creator_user_id = creator.id;

        const program = await this.programsService.createProgram(data);

        // todo populate program with some default exercises?

        return program;
    }

    async createNewExercise(data: IExercise, creatorUserId: number) {
        data.creator_user_id = creatorUserId;

        const exercise = await this.exercisesService.createExercise(data);

        return exercise;
    }

    async addExerciseToProgram(programId: number, exerciseId: number) {
        const exInPrInstance = await ExerciseInProgramModel.create({
            exercise_id: exerciseId,
            program_id: programId
        });

        return exInPrInstance.get();
    }
}