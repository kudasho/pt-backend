import 'reflect-metadata';
import { IExercise, IExerciseInProgram, IProgram, IUser } from '../typings';
import UserModel from '../models/user.model';

export interface IAuthService {
    getMe(): string;

    register(data: IUser): Promise<IUser>;

    login(email: string, password: string): Promise<IUser>;

    generateAuthToken(user: IUser): string;

    getUserByToken(token: string): Promise<IUser>;
}

export interface IUsersService {
    getUserWhere(where: IUser): Promise<IUser>;

    createUser(data: IUser): Promise<IUser>;
}

export interface IExercisesService {
    createExercise(data: IExercise): Promise<IExercise>;

    updateExerciseById(id: number, data: IExercise): Promise<IExercise>;

    deleteExerciseById(id: number): Promise<boolean>;

    getExerciseById(id: number): Promise<IExercise>;
}

export interface IProgramsService {
    createProgram(data: IProgram): Promise<IProgram>;

    updateProgramById(id: number, data: IProgram): Promise<IProgram>;

    getProgramById(id: number): Promise<IProgram>;

    deleteProgramById(id: number): Promise<boolean>;

    getProgramsList(owner: IUser): Promise<IProgram[]>;
}

export interface ITrainingService {
    startNewProgram(data: IProgram, user: IUser): Promise<IProgram>;

    createNewExercise(data: IExercise, creatorUserId: number): Promise<IExercise>;

    addExerciseToProgram(programId: number, exerciseId: number): Promise<IExerciseInProgram>;
}


let TYPES = {
    AuthService: Symbol('AuthService'),
    UsersService: Symbol('UsersService'),
    ExercisesService: Symbol('ExercisesService'),
    ProgramsService: Symbol('ProgramsService'),
    TrainingService: Symbol('TrainingService')
};

export default TYPES;