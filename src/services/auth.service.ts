import { inject, injectable } from 'inversify';

import * as passwordHash from 'password-hash';
import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { IAuthService, default as TYPES, IUsersService } from './types';
import UserModel from '../models/user.model';
import { IUser } from '../typings';

@injectable()
export class AuthService implements IAuthService {
    @inject(TYPES.UsersService) usersService: IUsersService;

    generateAuthToken(userObj) {
        return jwt.sign(userObj, config.get('JWT.secret'), {
            expiresIn: 60 * 60 * 24
        });
    }

    getUserByToken(tokenStr) {
        return new Promise((resolve, reject) => {
            jwt.verify(tokenStr, config.get('JWT.secret'), (err, user) => {
                if (err) {
                    return reject(err);
                }

                return resolve(user);
            });
        })
            .then((decodedUserObj: UserModel) => {
                return this.usersService.getUserWhere({id: decodedUserObj.id});
            });
    }

    getMe() {
        return 'Alexander';
    }

    register(data: IUser) {
        return this.usersService.createUser(data);
    }

    login(email, password) {
        return this.usersService.getUserWhere({email: email})
            .then(user => {
                if (!passwordHash.verify(password, user.password)) {
                    throw new Error('Wrong email or password');
                }

                delete user.password;

                return user;
            })
    }
}