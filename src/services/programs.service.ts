import { injectable } from 'inversify';
import { IProgramsService } from './types';

import ProgramModel from '../models/program.model';
import ExerciseModel from '../models/exercise.model';
import { IProgram, IUser } from '../typings';

@injectable()
export class ProgramsService implements IProgramsService {
    async createProgram(data: IProgram) {
        const programInstance = await ProgramModel.create(data);

        return programInstance.get();
    }

    async updateProgramById(id: number, data: IProgram) {
        const programInstance = await ProgramModel.findById(id);

        if (!programInstance) {
            throw new Error(`Program #${id} was not found`);
        }

        return programInstance.update(data);
    }

    async getProgramById(id: number) {
        const programInstance = await ProgramModel.findById(id, {
            include: [
                {
                    model: ExerciseModel
                }
            ]
        });

        if (!programInstance) {
            throw new Error(`Program #${id} was not found`);
        }

        return programInstance.get();
    }

    async deleteProgramById(id: number) {
        await ProgramModel.destroy({
            where: {
                id: id
            }
        });

        return true;
    }

    async getProgramsList(owner: IUser) {
        const programs = await ProgramModel.findAll({
            where: {
                creator_user_id: owner.id
            }
        });

        return programs;
    }
}