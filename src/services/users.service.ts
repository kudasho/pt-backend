import { injectable } from 'inversify';
import { IUsersService } from './types';
import UserModel from '../models/user.model';
import { IUser } from '../typings';

@injectable()
export class UsersService implements IUsersService {
    async getUserWhere(where: object) {
        const userInstance = await UserModel.findOne({
            where
        });

        if (!userInstance) {
            throw new Error(`User was not found`);
        }

        return userInstance.get();
    }

    async createUser(data: IUser) {
        const userInstance = await UserModel.create(data);

        return userInstance.get();
    }
}